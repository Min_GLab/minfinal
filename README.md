# MinFinal

The final assignment for Data Science Systems

## describes the code of this repository
The code of this repository is the final assignment about prediction of flight delay, the detail information is if the flight will be delayed due to weather when the customers are booking the flight to or from the busiest airports for domestic travel in the US.

## how to run it 
Jupyter Notebook with Python 3 

## what the user would expect if got the code running
The user will get a predict model for flight delay
